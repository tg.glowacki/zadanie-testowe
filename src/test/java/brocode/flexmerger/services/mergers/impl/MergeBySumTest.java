package brocode.flexmerger.services.mergers.impl;

import brocode.flexmerger.services.mergers.Merger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class MergeBySumTest {
    private Merger<Integer,Long> merger = new MergeBySum();

    @Test
    public void testSum() {
        //given
        List<Integer> numbers = Arrays.asList(10, 5, 3, 2);
        //when
        Long result = merger.merge(numbers);
        //then
        Assertions.assertThat(result).isEqualTo(20);
    }

    @Test
    public void testSumWithNegatives() {
        //given
        List<Integer> numbers = Arrays.asList(10, -5, -3, -2);
        //when
        Long result = merger.merge(numbers);
        //then
        Assertions.assertThat(result).isEqualTo(0);
    }

    @Test
    public void testSumAllNegatives() {
        //given
        List<Integer> numbers = Arrays.asList(-10, -5, -3, -2);
        //when
        Long result = merger.merge(numbers);
        //then
        Assertions.assertThat(result).isEqualTo(-20);
    }
}