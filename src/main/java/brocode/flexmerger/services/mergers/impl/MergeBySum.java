package brocode.flexmerger.services.mergers.impl;

import brocode.flexmerger.services.mergers.Merger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MergeBySum implements Merger<Integer, Long> {

    @Override
    public Long merge(List<Integer> sources) {
        return sources
                .stream()
                .mapToLong(s -> s)
                .sum();
    }

    @Override
    public Class<Integer> getSourceType() {
        return Integer.class;
    }
}
