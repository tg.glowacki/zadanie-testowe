package brocode.flexmerger.services.mergers.impl;

import brocode.flexmerger.services.mergers.Merger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MergeBySubtract implements Merger<Integer, Long> {
    @Override
    public Long merge(List<Integer> sources) {
        Long first = sources.get(0).longValue();
        int sum = sources.stream().skip(1).mapToInt(i -> i).sum();
        return first - sum;
    }

    @Override
    public Class<Integer> getSourceType() {
        return Integer.class;
    }


}
