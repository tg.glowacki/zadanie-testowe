package brocode.flexmerger.services.mergers.impl;

import brocode.flexmerger.services.mergers.Merger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MergeStrings implements Merger<String, String> {
    @Override
    public String merge(List<String> sources) {
        return sources
                .stream()
                .reduce("", String::concat);
    }
    @Override
    public Class<String> getSourceType() {
        return String.class;
    }
}
