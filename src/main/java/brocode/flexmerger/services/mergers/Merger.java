package brocode.flexmerger.services.mergers;

import java.util.List;

public interface Merger<S,T> {
    T merge(List<S> sources);
    Class<S> getSourceType();
    default String getName() {
        return getClass().getSimpleName();
    }

}
