package brocode.flexmerger.services.sources;

public interface Source<S> {
    S getValue();
    Class<S> getType();
    default String getName() {
        return getClass().getSimpleName();
    }

}
