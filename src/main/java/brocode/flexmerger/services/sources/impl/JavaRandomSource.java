package brocode.flexmerger.services.sources.impl;

import brocode.flexmerger.services.sources.Source;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class JavaRandomSource implements Source<Integer> {

    private Random source = new Random();

    @Override
    public Integer getValue() {
        return source.nextInt();
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
