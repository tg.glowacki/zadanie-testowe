package brocode.flexmerger.services.sources.impl;

import brocode.flexmerger.repositories.StoredValuesRepository;
import brocode.flexmerger.repositories.entities.StoredValues;
import brocode.flexmerger.services.sources.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSource implements Source<Integer> {

    @Autowired
    private StoredValuesRepository repository;

    @Override
    public Integer getValue() {
        StoredValues value = repository.findFirstByOrderByIdDesc();
        return value.getValue();
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
