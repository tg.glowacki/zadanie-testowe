package brocode.flexmerger.services.sources.impl;

import brocode.flexmerger.services.sources.Source;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RandomOrgSource implements Source<Integer> {

    final String uri = "https://www.random.org/integers/?num=1&min=-1000000000&max=1000000000&col=1&base=10&format=plain&rnd=new";

    RestTemplate restTemplate = new RestTemplate();

    @Override
    public Integer getValue() {
        String response = restTemplate.getForObject(uri, String.class).trim();
        return Integer.valueOf(response);
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
