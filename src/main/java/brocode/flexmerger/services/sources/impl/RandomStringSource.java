package brocode.flexmerger.services.sources.impl;

import brocode.flexmerger.services.sources.Source;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomStringSource implements Source<String> {
    private static String[] STRING = new String[]{"AAA", "BBB", "CCC", "DDD"};
    private Random rng = new Random();

    @Override
    public String getValue() {
        return STRING[rng.nextInt(STRING.length)];
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }
}
