package brocode.flexmerger.services;


import brocode.flexmerger.services.mergers.Merger;
import brocode.flexmerger.services.mergers.impl.MergeBySum;
import brocode.flexmerger.controllers.model.MergeResult;
import brocode.flexmerger.services.sources.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MergeService {
    private static final String DEFAULT_MERGER = MergeBySum.class.getSimpleName();

    private Merger merger;
    @Autowired
    private List<Merger> mergers;
    @Autowired
    private List<Source> dataSources;


    @PostConstruct
    private void init() throws Exception {
        changeMerger(DEFAULT_MERGER);
    }

    public MergeResult merge(final List<String> sources) {

        List components = sources
                .stream()
                .map(name -> dataSources
                        .stream()
                        .filter(s -> s.getName().equals(name))
                        .findFirst().get())
                .map(Source::getValue)
                .collect(Collectors.toList());

        String operand = merger.getName();

        Object result = this.merger.merge(components);


        return new MergeResult(result, operand, components);
    }

    public List<String> getSourceNames() {
        return dataSources
                .stream()
                .map(Source::getName)
                .collect(Collectors.toList());
    }

    public List<String> getMergersNames() {
        return mergers
                .stream()
                .map(Merger::getName)
                .collect(Collectors.toList());
    }

    public boolean changeMerger(String mergerId) {
        Optional<Merger> result = mergers
                .stream().filter(f -> mergerId.equals(f.getName()))
                .findFirst();
        if (result.isEmpty()) {
            return false;
        }
        merger = result.get();
        return true;
    }

    public boolean validate(List<String> sourceNames) {
        return !dataSources
                .stream()
                .filter(ds -> sourceNames.contains(ds.getName())
                        && !merger.getSourceType().equals(ds.getType()))
                .findAny()
                .isPresent();
    }

    public String getSelectedMergerName() {
        return merger.getName();
    }
}
