package brocode.flexmerger.controllers.exception;

import java.io.IOException;

public class InvalidMergerNameException extends IOException {
    public InvalidMergerNameException(String mergerId) {
        super("Merger with id " + mergerId + " does not exists");
    }
}
