package brocode.flexmerger.controllers.exception;

import java.io.IOException;

public class InvalidSourceException extends IOException {
    public InvalidSourceException(String sourceName) {
        super("No source with name " + sourceName);
    }
}
