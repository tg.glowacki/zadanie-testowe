package brocode.flexmerger.controllers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.directory.InvalidAttributeValueException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@ControllerAdvice
public class MergeServiceValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidAttributeValueException.class)
    public void handleInvalidAttributeValueException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(SourceNotAcceptableForMergerException.class)
    public void handleSourceNotAcceptableForMergerException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(InvalidMergerNameException.class)
    public void handleInvalidMergerNameException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(InvalidSourceException.class)
    public void handleInvalidSourceException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }
}


