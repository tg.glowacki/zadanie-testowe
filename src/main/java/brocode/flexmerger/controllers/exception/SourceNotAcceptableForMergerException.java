package brocode.flexmerger.controllers.exception;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SourceNotAcceptableForMergerException extends IOException {
    public SourceNotAcceptableForMergerException(String mergerName, List<String> sourceNames) {
        super("One or more Source " + sourceNames + " type is not acceptable for selected merger: " + mergerName);
    }
}
