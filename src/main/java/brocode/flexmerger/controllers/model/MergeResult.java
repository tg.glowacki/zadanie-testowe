package brocode.flexmerger.controllers.model;

import java.util.Collections;
import java.util.List;

public class MergeResult<T> {

    private T result;
    private String mergeOperator;
    private List<Integer> components;


    public MergeResult(T result, String mergeOperator, List<Integer> components) {

        this.result = result;
        this.mergeOperator = mergeOperator;
        this.components = components;

    }

    public T getResult() {
        return result;
    }

    public String getMergeOperator() {
        return mergeOperator;
    }

    public List<Object> getComponents() {
        return Collections.unmodifiableList(components);
    }
}
