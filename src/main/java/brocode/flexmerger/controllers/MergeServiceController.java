package brocode.flexmerger.controllers;

import brocode.flexmerger.controllers.exception.InvalidMergerNameException;
import brocode.flexmerger.controllers.model.MergeResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


public interface MergeServiceController {
    @GetMapping(value = "/merge/{first}/{second}", produces = MediaType.APPLICATION_JSON_VALUE)
    MergeResult mergeSoruces(@PathVariable("first") String first, @PathVariable("second") String second) throws Exception;

    @GetMapping(value = "/merge/mergers")
    List<String> getMergers();

    @PutMapping(value = "/merge/merger/{value}")
    @ResponseStatus(HttpStatus.OK)
    void changeMerger(@PathVariable("value") String mergerId) throws InvalidMergerNameException;

}
