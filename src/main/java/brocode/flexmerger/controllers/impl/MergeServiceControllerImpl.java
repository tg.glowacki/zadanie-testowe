package brocode.flexmerger.controllers.impl;

import brocode.flexmerger.controllers.exception.InvalidMergerNameException;
import brocode.flexmerger.controllers.exception.InvalidSourceException;
import brocode.flexmerger.controllers.exception.SourceNotAcceptableForMergerException;
import brocode.flexmerger.services.MergeService;
import brocode.flexmerger.controllers.model.MergeResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@RestController
public class MergeServiceControllerImpl implements brocode.flexmerger.controllers.MergeServiceController {

    @Autowired
    private MergeService service;

    @Override
    public MergeResult mergeSoruces(String first, String second) throws IOException {
        List<String> sourceNames = service.getSourceNames();
        List<String> selectedSourceNames = Arrays.asList(first, second);

        if (!sourceNames.contains(first)) {
            throw new InvalidSourceException(first);
        } else if (!sourceNames.contains(second)) {
            throw new InvalidSourceException(second);
        }

        if (!service.validate(selectedSourceNames)) {
            throw new SourceNotAcceptableForMergerException( service.getSelectedMergerName(),selectedSourceNames);
        }

        return service.merge(selectedSourceNames);
    }

    @Override
    public List<String> getMergers() {
        return service.getMergersNames();
    }

    @Override
    public void changeMerger(String mergerId) throws InvalidMergerNameException {
        if (service.getMergersNames().contains(mergerId))
            service.changeMerger(mergerId);
        else
            throw new InvalidMergerNameException(mergerId);
    }


}
