package brocode.flexmerger.controllers.impl;

import brocode.flexmerger.controllers.SourcesController;
import brocode.flexmerger.services.MergeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SourcesControllerImpl implements SourcesController {
    @Autowired
    private MergeService service;

    @Override
    public List<String> getSourcesNames() {
        return service.getSourceNames();
    }
}
