package brocode.flexmerger.controllers.impl;

import brocode.flexmerger.controllers.StoredValuesController;
import brocode.flexmerger.repositories.StoredValuesRepository;
import brocode.flexmerger.repositories.entities.StoredValues;
import brocode.flexmerger.repositories.entities.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
public class StoredValuesControllerImpl implements StoredValuesController {
    @Autowired
    private StoredValuesRepository repository;

    @Override
    public void add(Integer value) {
        repository.save(new StoredValues(value));
    }

    @Override
    public void add() throws Exception {
        add(new Random().nextInt());
    }

    @Override
    public List<Value> getAll() {
        return repository.findBy();
    }
}
