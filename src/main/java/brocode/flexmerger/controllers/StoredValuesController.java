package brocode.flexmerger.controllers;

import brocode.flexmerger.repositories.entities.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

public interface StoredValuesController {

    @PostMapping(value = "/storedValues/{value}")
    @ResponseStatus(HttpStatus.CREATED)
    void add(@PathVariable("value") Integer value);

    @PostMapping(value = "/storedValues")
    @ResponseStatus(HttpStatus.CREATED)
    void add() throws Exception;

    @GetMapping("/storedValues")
    List<Value> getAll();
}
