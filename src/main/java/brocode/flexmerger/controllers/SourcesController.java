package brocode.flexmerger.controllers;

import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface SourcesController {
    @GetMapping("/sources")
    List<String> getSourcesNames();
}
