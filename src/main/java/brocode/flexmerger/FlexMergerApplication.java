package brocode.flexmerger;

import brocode.flexmerger.repositories.StoredValuesRepository;
import brocode.flexmerger.repositories.entities.StoredValues;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FlexMergerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlexMergerApplication.class, args);
    }

    @Bean
    public CommandLineRunner initDB(StoredValuesRepository repository) {
        return args -> {
            repository.save(new StoredValues(12345));
            repository.save(new StoredValues(55555));
        };
    }
}
