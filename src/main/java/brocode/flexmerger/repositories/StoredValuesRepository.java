package brocode.flexmerger.repositories;

import brocode.flexmerger.repositories.entities.StoredValues;
import brocode.flexmerger.repositories.entities.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface StoredValuesRepository extends JpaRepository<StoredValues, Long> {
    StoredValues findFirstByOrderByIdDesc();

    List<Value> findBy();
}
